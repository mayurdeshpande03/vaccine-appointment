
const request =require('request');
var cron = require('node-cron');
const notifier = require('node-notifier');
const nodemailer = require('nodemailer');
const prompt = require("prompt-sync")({ sigint: true });
const STATES = require('./states.json');

const state_name = prompt("Enter your state: ");
const state_id = STATES.find((state) => {

    if (state_name.localeCompare(state.state_name) === 0) {
        return state.state_name === state_name;
    }
});

const district_name = prompt("Enter your district: ");

request(`https://cdn-api.co-vin.in/api/v2/admin/location/districts/${state_id.state_id}`,async (error, response, html)=> {
    
    const DISTRICTS = JSON.parse(JSON.stringify(JSON.parse(response.body).districts));
    const district_id = DISTRICTS.find((district) => {
            return district.district_name === district_name;
    });


    let mailTransporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'yourmail@gmail.com',
            pass: 'password'
        }
    });
    
    let mailDetails = {
        from: 'yourmail@gmail.com',
        to: 'receiver@gmail.com',
        subject: 'Vaccine Available! Book your appointment asap!',
        html: ''
    };
    
    let job = cron.schedule('* * * * * *', () => {
    request(`https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id=${district_id.district_id}&date=15-05-2021`,async (error, response, html)=>{
        if (error) {
                console.log(error);
            } else {
                let flag = false;
                if (response && response.statusCode === 200) {
                    let data = JSON.parse((response.body)).centers;
                    for (let i in data) {

                        let count = await data[i].sessions.filter((entry) => entry.available_capacity >= 0);
                        if (count && count.length) {

                            notifier.notify({
                                title: 'Vaccine Available',
                                message: data[i].name,
                                sound: true
                            });
                            
                            console.log('Here is the slot', data[i]);
                            flag = true;

                            mailDetails['html'] = `
                            <!DOCTYPE html>
                            <html>
                            <body>
                            
                            <h1>Center Details</h1>
                            <h3>Center Name: ${data[i].name}</h3>
                            <h3>Center Address: ${data[i].address}</h3>
                            <h3>From: ${data[i].from}  To: ${data[i].to}</h3>
                            
                            <br>
                            
                            <h1>Vaccine Details</h1>
                            <h3>Vaccine Name: ${count[0].vaccine}</h3>
                            <h3>Minimum Age Limit: ${count[0].min_age_limit}</h3>

                            </body>
                            </html>
                            `;

                            mailTransporter.sendMail(mailDetails, function(err, data) {
                                if(err) {
                                    console.log('Error Occurs', err);
                                } else {
                                    console.log('Email sent successfully');
                                }
                            })   ;
                        }
                    }
                }
                if (flag === true) {
                    job.stop();
                } else {
                    console.log('wait ...');
                }
            }
        });
    });
});
